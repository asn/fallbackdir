use anyhow::Result;
use chrono::Utc;
use rand::seq::SliceRandom;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::sync::Arc;

use tor_client;
use tor_dirmgr;
use tor_netdir;
use tor_netdoc::doc::netstatus::RouterFlags;

fn write_relay_to_file(writer: &mut BufWriter<&File>, relay: &tor_netdir::Relay) -> Result<()> {
    let ipv4: Vec<_> = relay
        .rs()
        .orport_addrs()
        .filter(|sa| sa.is_ipv4())
        .collect();
    let ipv6: Vec<_> = relay
        .rs()
        .orport_addrs()
        .filter(|sa| sa.is_ipv6())
        .collect();
    let mut ipv6_str: String = "".to_string();
    if !ipv6.is_empty() {
        ipv6_str = format!(" ipv6={}", ipv6[0]);
    }
    writeln!(writer, "/* nickname={} */", relay.rs().nickname())?;
    writeln!(
        writer,
        "\"{} orport={}{}\"",
        ipv4[0].ip(),
        ipv4[0].port(),
        ipv6_str
    )?;
    writeln!(
        writer,
        "\" id={}\",",
        relay.rsa_id().to_string().to_uppercase().replace("$", "")
    )?;
    writeln!(writer, "\" ed={}\",", relay.md().ed25519_id().to_string())?;
    writeln!(writer, "")?;
    Ok(())
}

fn main() -> Result<()> {
    let mut builder = tor_dirmgr::NetDirConfigBuilder::new();
    builder.use_default_cache_path()?;
    let config: tor_dirmgr::NetDirConfig = builder.finalize()?;

    tor_rtcompat::task::block_on(async {
        println!("[+] Bootstrapping to the Tor network");
        let tor_client = Arc::new(tor_client::TorClient::bootstrap(config).await?);
        println!("[+] Searching relays");
        let netdir = tor_client.dirmgr().netdir();
        let relays: Vec<_> = netdir
            .relays()
            .filter(|r| {
                r.is_dir_cache() &&
                r.rs().flags().contains(RouterFlags::FAST)
                    && r.rs().flags().contains(RouterFlags::STABLE)
            })
            .collect();

        let picks = relays.choose_multiple(&mut rand::thread_rng(), 200);

        let file = File::create("fallback_dirs.inc")?;
        let mut writer = BufWriter::new(&file);

        writeln!(writer, "/* type=fallback */")?;
        writeln!(writer, "/* last-updated={} */", Utc::now().to_rfc2822())?;
        writeln!(writer, "")?;

        for relay in picks {
            write_relay_to_file(&mut writer, &relay)?;
        }
        Ok(())
    })
}
